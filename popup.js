// PDFJS.workerSrc = "../build/pdf.worker.js";
let user_id = null;
let folder = null;
let folder_id = 0;
chrome.storage.sync.get(["session_id", "folder", "folder_id"], function(result) {
  if (
    result.session_id != undefined &&
    result.session_id != null &&
    parseInt(result.session_id) > 0
  ) {
    user_id = result.session_id;
    folder = result.folder;
    folder_id = result.folder_id;
    const comment_input_wrap = `
        <div class="col-md-12 input-wrap shadow-sm p-0">
            <div class="form-group p-0 mb-1 mt-2">
                <textarea id="comment" class="form-control" placeholder="Enter your comments"></textarea>
            </div>
            <div class="form-group mb-0 pr-0 text-right">
                <button class="btn btn-primary btn-sm btn-submit-comment">Submit</button>
                <button class="btn btn-secondary btn-sm btn-cancel-comment">Cancel</button>
            </div>
        </div>`;

    const init = async () => {
      $(".folder_name").html(
        decodeURIComponent((folder + "").replace(/\+/g, "%20"))
      );
      let result = await fetch(
        `https://ai2squared.com/api/folders/list/user/${user_id}`
      );
      let { folders } = await result.json();

      var tree = folders.map((item, index) => {
        return `<li class="list-group-item node-tree ${folder_id == item.id ? 'active' : ''}" data-nodeid="${index}" data-folder_id="${item.id}">
          <span class="icon icon-folder-alt text-white glyphicon"></span>
          <span class="icon icon-folder-alt text-white node-icon fa fa-folder fa-lg mr-2"></span>
          ${item.name}
        </li>`;
      });

      // var tree = folders.map(item => {
      //   return { text: item.name };
      // });

      // $("#tree").treeview({
      //   data: tree,
      //   backColor: "#f9fbfd",
      //   showBorder: false,
      //   showIcon: true,
      //   nodeIcon: "fa fa-folder fa-lg mr-2"
      // });

      $("#tree").html(`<ul class="list-group">${tree}</ul>`);

      // setTimeout(() => {
      //   $("#tree li").each(function() {
      //     var folder_name = $(this).text();
      //     if (
      //       folder_name ==
      //       decodeURIComponent((folder + "").replace(/\+/g, "%20"))
      //     ) {
      //       $(this).addClass("active");
      //     }
      //   });
      // }, 1000);

      const urlParams = new URLSearchParams(window.location.search);
      const url = urlParams.get("url");

      result = await fetch("https://ai2squared.com/api/highlights/getByURL", {
        method: "POST",
        headers: {
          "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
        },
        body: `url=${url}&folder=${
          folder != null ? folder : folders[0].name
        }&folder_id=${folder_id > 0 ? folder_id : folders[0].id}&from=popup`
        // body: `url=${url}`
      });
      let { highlights } = (await result.json()) || [];

      // console.log("=========>", highlights);
      $(".site_title").html("");
      $(".total_highlights").html(0);
      $(".highlight_colors_circles").html("");
      $("#highlights_wrap").html("");

      if (highlights.length) {
        $(".site_title").html(highlights[0].page_title);
        $(".total_highlights").html(highlights.length);
        let color_circles = "";
        let list_html = "";
        highlights.forEach(element => {
          if (element.color != undefined) {
            color_circles += `<span class="color-circle" style="background-color: ${element.color.color_code};"></span>`;
          }
          list_html += `
      <div class="col-md-12 p-1 highlight-wrap" style="border-left: 5px solid ${element.color.color_code};" data-id="${element.id}">
        <div class="highlight-name">${element.content}</div>
        <div class="comments-wrap">`;
          let comments_html = "";

          element.comments.forEach(comment => {
            let img_url = `https://ui-avatars.com/api/?name=${encodeURI(comment.user.name)}`;
            if(comment.user.profile_pic != null)
              img_url = comment.user.profile_pic;
            comments_html += `<div class="col-md-12 comment-wrap shadow-sm mt-2 mb-2">
            <div class="col-md-12 p-0">
              <img src="${img_url}" class="rounded-circle" width="25" /> ${comment.user.name}
            </div>
          <p class="comment_text text-muted">
          ${comment.comment}
          </p>
          <div class="col-md-12 text-right pb-2 pr-0">
            <a
              href="javascript:void(0);"
              class="btn btn-success btn-sm btn_like"
              data-id="${comment.id}"
              ><i class="fa fa-thumbs-up"></i> (${comment.likes.length})</a
            >
            <a
              href="javascript:void(0);"
              class="btn btn-danger btn-sm btn_dislike"
              data-id="${comment.id}"
              ><i class="fa fa-thumbs-down"></i> (${comment.dislikes.length})</a
            >
          </div>
        </div>`;
          });

          list_html += `${comments_html}</div></div>`;
        });
        $(".highlight_colors_circles").html(color_circles);
        $("#highlights_wrap").html(list_html);
      }
    };

    // init();

    chrome.runtime.onMessage.addListener((msg, sender, response) => {
      if (msg.from == "content" && msg.subject == "loadHighlights") {
        init();
      }
    });

    $(document).on("click", ".highlight-name", function() {
      var id = $(this)
        .closest(".highlight-wrap")
        .data("id");
      $(".input-wrap").remove();

      $(this)
        .closest(".highlight-wrap")
        .append(comment_input_wrap);
    });

    $(document).on("click", ".btn-cancel-comment", function() {
      $(".input-wrap").remove();
    });

    $(document).on("click", ".btn-submit-comment", async function() {
      var comment = $("#comment").val();
      var highlight_id = $(this)
        .closest(".highlight-wrap")
        .data("id");
      var user_id = result.session_id;

      let new_comment = await fetch(
        "https://ai2squared.com/api/highlight-comments",
        {
          method: "POST",
          headers: {
            "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
          },
          body: `highlight_id=${highlight_id}&comment=${comment}&user_id=${user_id}`
        }
      );

      init();
    });

    $(document).on("click", ".btn_like", async function() {
      var comment_id = $(this).data("id");
      var user_id = result.session_id;
      var like = 1;

      let new_like = await fetch(
        "https://ai2squared.com/api/highlight-comments/like",
        {
          method: "POST",
          headers: {
            "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
          },
          body: `comment_id=${comment_id}&user_id=${user_id}&like=${like}`
        }
      );

      init();
    });

    $(document).on("click", ".btn_dislike", async function() {
      var comment_id = $(this).data("id");
      var user_id = result.session_id;
      var like = 0;

      let new_like = await fetch(
        "https://ai2squared.com/api/highlight-comments/like",
        {
          method: "POST",
          headers: {
            "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
          },
          body: `comment_id=${comment_id}&user_id=${user_id}&like=${like}`
        }
      );

      init();
    });

    $(document).on("click", ".ext-draw-folders", function() {
      var leftPane = $(".siderbar-folder");
      if (leftPane.is(":visible")) {
        leftPane.hide();
      } else {
        leftPane.show();
      }
    });

    $(document).on("click", "li.node-tree", function() {
      chrome.storage.sync.set({ folder: $(this).text(), folder_id: $(this).data('folder_id') });
      folder = $(this).text();
      folder_id = $(this).data('folder_id');
      init();
    });
  }
});
