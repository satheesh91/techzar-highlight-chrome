const Selection = (function() {
  function copyTextToClipboard(text) {
    let textArea = document.createElement("textarea");
    textArea.style.position = "fixed";
    textArea.style.top = 0;
    textArea.style.left = 0;
    textArea.style.width = "2em";
    textArea.style.height = "2em";
    textArea.style.padding = 0;
    textArea.style.border = "none";
    textArea.style.outline = "none";
    textArea.style.boxShadow = "none";
    textArea.style.background = "transparent";
    textArea.value = text;

    document.body.appendChild(textArea);
    textArea.focus();
    textArea.select();

    try {
      let successful = document.execCommand("copy");
      let msg = successful ? "successful" : "unsuccessful";
      // console.log("Copying text command was " + text);
    } catch (err) {
      //console.log('Oops, unable to copy');
    }

    document.body.removeChild(textArea);
  }

  function _selection() {
    const menu = {
      colorOne: true,
      colorTwo: true,
      colorThree: true,
      colorFour: true,
      colorFive: true
    };

    const cOneConfig = {
      url: "",
      icon: '<button class="techzarBtn techzarbtn1"></button>'
    };

    const cTwoConfig = {
      url: "",
      icon: '<button class="techzarBtn techzarbtn2"></button>'
    };

    const cThreeConfig = {
      url: "",
      icon: '<button class="techzarBtn techzarbtn3"></button>'
    };

    const cFourConfig = {
      url: "",
      icon: '<button class="techzarBtn techzarbtn4"></button>'
    };

    const cFiveConfig = {
      url: "",
      icon: '<button class="techzarBtn techzarbtn5"></button>'
    };

    let selection = "";
    let text = "";
    let bgcolor = "crimson";
    let iconcolor = "#fff";

    let _icons = {};
    let arrowsize = 5;
    let buttonmargin = 7 * 2;
    let iconsize = 24 + buttonmargin;
    let top = 0;
    let left = 0;

    function cOneButton() {
      const cOnebtn = new Button(cOneConfig.icon, function() {
        copyTextToClipboard(text);
      });
      return cOnebtn;
    }

    function cTwoButton() {
      const cTwobtn = new Button(cTwoConfig.icon, function() {
        copyTextToClipboard(text);
      });
      return cTwobtn;
    }

    function cThreeButton() {
      const cThreebtn = new Button(cThreeConfig.icon, function() {
        copyTextToClipboard(text);
      });
      return cThreebtn;
    }

    function cFourButton() {
      const cFourbtn = new Button(cFourConfig.icon, function() {
        copyTextToClipboard(text);
      });
      return cFourbtn;
    }

    function cFiveButton() {
      const cFivebtn = new Button(cFiveConfig.icon, function() {
        copyTextToClipboard(text);
      });
      return cFivebtn;
    }

    function IconStyle() {
      const style = document.createElement("style");
      style.innerHTML = `.selection__icon{fill:${iconcolor};}`;
      document.body.appendChild(style);
    }

    function appendIcons() {
      const myitems = [
        { feature: "colorOne", call: cOneButton() },
        { feature: "colorTwo", call: cTwoButton() },
        { feature: "colorThree", call: cThreeButton() },
        { feature: "colorFour", call: cFourButton() },
        { feature: "colorFive", call: cFiveButton() }
      ];
      const div = document.createElement("div");
      let count = 0;
      myitems.forEach(item => {
        if (menu[item.feature]) {
          div.appendChild(item.call);
          count++;
        }
      });
      return {
        icons: div,
        length: count
      };
    }

    function setTooltipPosition() {
      const position = selection.getRangeAt(0).getBoundingClientRect();
      const DOCUMENT_SCROLL_TOP =
        window.pageXOffset ||
        document.documentElement.scrollTop ||
        document.body.scrollTop;
      // top = position.top + DOCUMENT_SCROLL_TOP - iconsize - arrowsize;
      // // left = position.left + (position.width - iconsize * _icons.length) / 2;
      // left =
      //   position.left +
      //   (position.width - iconsize * $(".selection div").length) / 2;
      top = position.top + DOCUMENT_SCROLL_TOP - 130;
      left = position.x;
      $(".selection").css("display", "");
    }

    function moveTooltip() {
      setTooltipPosition();
      let tooltip = document.querySelector(".selection");
      tooltip.style.top = `${top}px`;
      tooltip.style.left = `${left}px`;
    }

    function drawTooltip() {
      _icons = appendIcons();
      setTooltipPosition();

      const div = document.createElement("div");
      div.className = "selection";
      div.style =
        "line-height:0;" +
        "position:absolute;" +
        "background-color:" +
        bgcolor +
        ";" +
        "border-radius:20px;" +
        "top:" +
        top +
        "px;" +
        "left:" +
        left +
        "px;" +
        "transition:all .2s ease-in-out;" +
        "box-shadow: 0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22);" +
        "z-index:99999;";

      div.appendChild(_icons.icons);

      const arrow = document.createElement("div");
      arrow.style =
        "position:absolute;" +
        "border-left:" +
        arrowsize +
        "px solid transparent;" +
        "border-right:" +
        arrowsize +
        "px solid transparent;" +
        "border-top:" +
        arrowsize +
        "px solid " +
        bgcolor +
        ";" +
        "bottom:-" +
        (arrowsize - 1) +
        "px;" +
        "left:" +
        ((iconsize * _icons.length) / 2 - arrowsize) +
        "px;" +
        "width:0;" +
        "height:0;";

      if (!menu.disable) {
        div.appendChild(arrow);
      }
      document.body.appendChild(div);
    }

    function attachEvents() {
      function hasSelection() {
        return !!window.getSelection().toString();
      }

      function hasTooltipDrawn() {
        return !!document.querySelector(".selection");
      }

      window.addEventListener(
        "mouseup",
        function(e) {
          chrome.storage.sync.get(["session_id", "folder", "folder_id"], function(store) {
            $.ajax({
              // url: `https://ai2squared.com/api/folders/${encodeURI(
              //   store.folder
              // )}`,
              url: `https://ai2squared.com/api/folders/id/${store.folder_id}`,
              headers: { Accept: "application/json" },
              dataType: "json",
              success: function(res) {
                setTimeout(function mouseTimeout() {
                  if (hasTooltipDrawn()) {
                    if (hasSelection()) {
                      if ($(e.target).parents(".selection").length == 0) {
                        selection = window.getSelection();
                        text = selection.toString();
                        moveTooltip();
                        $("#tags1").val(text);
                      }
                      return;
                    } else {
                      if ($(e.target).parents(".selection").length == 0) {
                        $(".selection").css("display", "none");
                      }
                    }
                  }
                  if (hasSelection()) {
                    selection = window.getSelection();
                    text = selection.toString();
                    // drawTooltip();
                    if ($(e.target).parents(".selection").length == 0) {
                      setTooltipPosition();
                      $("#tags1").val(text);

                      //$("#tags1").focus();
                    }
                  }

                  var html = "";
                  res.folder.colors.forEach((item, index) => {
                    html += `
                      .techzarbtn${item.id}{
                        background-color: ${item.color_code.toUpperCase()} !important;
                        border-color: ${item.color_code.toUpperCase()} !important;
                      }
                      .techzarfill${item.id}{
                        background-color: ${item.color_code.toUpperCase()} !important;
                      }
                    `;
                    $(".techzarbtn" + item.id).addClass(`techzarbtn${item.id}`);
                    $(".techzarbtn" + item.id).data("folder_id", res.folder.id);
                    $(".techzarbtn" + item.id).data(
                      "fill_class",
                      `techzarfill${item.id}`
                    );
                    $(".techzarbtn" + item.id).data("color_id", `${item.id}`);
                  });
                  // $("head").append(`<style>${html}</style>`);
                }, 10);
              },
              error: function(err) {
                console.log(err);
              }
            });
          });
        },
        false
      );
    }

    $(document).on("click", "#searchDiv-btn", function() {
      var l = $("#tags1").val().length;
      console.log(l);
      //alert('haj');
      //$("#tags1").val(text);
      var value = $("#tags1").val();
      console.log(value);
      $.ajax({
        url: "https://ai2squared.com/api/search",
        type: "POST",
        data: {
          search: value,
          user_id: 1
        },
        success: function(res) {
          $("#searchDiv").empty();
          //$('#8110').css("padding: 10px 14px;height:300px!important;overflow-y: scroll;");
          var availableTags = [];
          for (var i = 0; i < res.length; i++) {
            //alert(res[i].content);
            console.log(res[i].content);
            availableTags.push(res[i].content + " page_url:" + res[i].page_url);
            $("#searchDiv").append(
              '<p style="margin-top: 0.8em!important;margin-bottom: 0.8em!important;font-size: 15px!important;"><a style="color:black!important;" href="' +
                res[i].page_url +
                '" target="_blank">' +
                res[i].content.slice(0, 30) +
                "</a></p><br><br>"
            );
          }
        },
        error: function(err) {
          console.log(err);
        }
      });
    });

    function config(options) {
      menu.colorOne =
        options.colorOne === undefined ? menu.colorOne : options.colorOne;
      menu.colorTwo =
        options.colorTwo === undefined ? menu.colorTwo : options.colorTwo;
      menu.colorThree =
        options.colorThree === undefined ? menu.colorThree : options.colorThree;
      menu.colorFour =
        options.colorFour === undefined ? menu.colorFour : options.colorFour;
      menu.colorFive =
        options.colorFive === undefined ? menu.colorFive : options.colorFive;

      bgcolor = options.backgroundColor || "#333";
      iconcolor = options.iconColor || "#fff";
      return this;
    }

    function init() {
      IconStyle();
      attachEvents();
      return this;
    }

    return {
      config: config,
      init: init
    };
  }

  function Button(icon, clickFn) {
    const btn = document.createElement("div");
    btn.style =
      "display:inline-block;" +
      "margin:7px;" +
      "cursor:pointer;" +
      "transition:all .2s ease-in-out;";
    btn.innerHTML = icon;
    btn.onclick = clickFn;
    btn.onmouseover = function() {
      this.style.transform = "scale(1.2)";
    };
    btn.onmouseout = function() {
      this.style.transform = "scale(1)";
    };
    return btn;
  }

  return _selection;
})();
